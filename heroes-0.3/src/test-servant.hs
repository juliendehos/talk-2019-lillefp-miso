import Common
import Network.HTTP.Client (defaultManagerSettings, newManager)
import Servant.API
import Servant.Client
import Servant

main :: IO ()
main = do
    manager <- newManager defaultManagerSettings
    let env = mkClientEnv manager (BaseUrl Http "localhost" 3000 "")
    runClientM (fetchAdd 20 22) env >>= print
    runClientM fetchHeroes env >>= print

