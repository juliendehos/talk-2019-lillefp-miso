# Isomorphic web apps in Haskell

[LilleFP12 2019-02-27](https://www.meetup.com/fr-FR/Lille-FP/events/258682124/)

[Slides](https://juliendehos.gitlab.io/talk-2019-lillefp-miso)

[A more detailled introduction to Haskell](https://juliendehos.gitlab.io/talk-2019-lillefp-miso/full-intro.html)

[Another Miso example, with CI/CD](https://gitlab.com/juliendehos/miso-xhr)


## running heroes-... examples

install [Nix](https://nixos.org/nix/) then

```
make
firefox "localhost:3000"
```

or

```
# in a first shell
nix-shell -A client
make client
...

# in a second shell
nix-shell -A server
make server
```

