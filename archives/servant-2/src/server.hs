{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

import Common
import qualified Data.Text as T
import Network.Wai.Handler.Warp (run)
import Servant

server :: Server Api
server 
    =    return (Message "ok")
    :<|> return . (*2)
    :<|> return . sayHello

sayHello :: Maybe T.Text -> T.Text
sayHello Nothing = "Hello anonymous"
sayHello (Just name) = T.concat ["Hello ", name]

main :: IO ()
main = run 3000 $ serve api server

