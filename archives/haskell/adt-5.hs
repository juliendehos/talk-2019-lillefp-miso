
-- parametric type

data Rectangle a = Rectangle a a
    deriving (Show)

r1 :: Rectangle Int
r1 = Rectangle 4 2

r2 :: Rectangle Double
r2 = Rectangle 42 13.37

main = do
    print r1
    print r2

