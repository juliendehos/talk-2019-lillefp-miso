
data Rectangle a = Rectangle a a

main = print $ Rectangle 4 2

instance Show a => Show (Rectangle a) where
    show (Rectangle w h) = "Rectangle " ++ show w ++ " " ++ show h

