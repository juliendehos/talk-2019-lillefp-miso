
-- product type

data Rectangle = Rectangle Double Double

getArea :: Rectangle -> Double
getArea (Rectangle w h) = w*h

main = print $ getArea $ Rectangle 4 2

