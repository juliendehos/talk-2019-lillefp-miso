
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just $ sqrt x else Nothing

mul2 :: Double -> Double
mul2 = (*2)

safeMul2 :: Maybe Double -> Maybe Double
safeMul2 = fmap mul2

main :: IO ()
main = do
    print $ safeMul2 $ safeSqrt (-1)
    print $ safeMul2 $ safeSqrt 441

