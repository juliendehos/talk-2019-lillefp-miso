
safeSqrt :: Double -> Maybe Double
safeSqrt x = if x > 0 then Just (sqrt x) else Nothing

main = do
    print $ (*2) <$> safeSqrt 441
    print $ (*) <$> safeSqrt 441 <*> Just 2
    print $ Just (*2) <*> safeSqrt 441 

