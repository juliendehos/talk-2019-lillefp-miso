
-- record

data Rect = Rect
    { rectWidth  :: Double
    , height_    :: Double
    } deriving (Show)

r1 = Rect 4 2

r2 = Rect { height_ = 2, rectWidth = 4 }

r3 = r1 { rectWidth = 42 }

w3 = rectWidth r3

main = do
    print r1
    print r2
    print r3
    print w3

