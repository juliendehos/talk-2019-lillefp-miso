{-# LANGUAGE OverloadedStrings #-}

import           Miso
import qualified Miso.String as MS

type Model 
    = Int

data Action
    = NoOp
    | AddOne
    | SubOne
    deriving (Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel AddOne m = noEff (m + 1)
updateModel SubOne m = noEff (m - 1)

viewModel :: Model -> View Action
viewModel m = div_ []
    [ h1_ [] [ text "miso-2" ]
    , p_ [] 
        [ button_ [ onClick SubOne ] [ text "-" ]
        , text (MS.ms m)
        , button_ [ onClick AddOne ] [ text "+" ]
        ]
    ]

main :: IO ()
main = startApp App 
    { initialAction = NoOp
    , model = 0
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = []
    , mountPoint = Nothing
    }

