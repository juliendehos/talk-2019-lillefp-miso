{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

import Data.Aeson
import qualified Data.Text as T
import GHC.Generics
import Network.Wai.Handler.Warp (run)
import Servant

data Message = Message 
    { msg :: T.Text 
    } deriving (Generic)

instance ToJSON Message

sayHello :: Maybe T.Text -> T.Text
sayHello Nothing = "Hello anonymous"
sayHello (Just name) = T.concat ["Hello ", name]

type Api 
    =    "message" :> Get '[JSON] Message
    :<|> "double" :> Capture "nb" Int :> Get '[JSON] Int
    :<|> "hello" :> QueryParam "name" T.Text :> Get '[PlainText] T.Text

server :: Server Api
server 
    =    return (Message "ok")
    :<|> return . (*2)
    :<|> return . sayHello

main :: IO ()
main = run 3000 $ serve (Proxy :: Proxy Api) server

