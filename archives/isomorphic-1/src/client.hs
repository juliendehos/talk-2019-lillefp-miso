{-# LANGUAGE OverloadedStrings          #-}

import Miso
import Miso.String

main :: IO ()
main = startApp App
    { initialAction = NoOp
    , model = Model 0
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = []
    , mountPoint = Nothing
    }

data Model = Model 
    { counter_ :: Int
    } deriving (Eq)

data Action
    = NoOp
    | SubOne
    | AddOne
    deriving (Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel SubOne m = noEff m { counter_ = counter_ m - 1 }
updateModel AddOne m = noEff m { counter_ = counter_ m + 1 }  

viewModel :: Model -> View Action
viewModel (Model c) = div_ []
    [ h1_ [] [ text "counter" ]
    , p_ []
        [ button_ [ onClick SubOne ] [ text "-" ]
        , text $ ms $ show c
        , button_ [ onClick AddOne ] [ text "+" ]
        ]
    ]

