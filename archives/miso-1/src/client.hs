{-# LANGUAGE OverloadedStrings #-}

import           Miso
import qualified Miso.String as MS

type Model 
    = MS.MisoString

data Action
    = NoOp
    deriving (Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m

viewModel :: Model -> View Action
viewModel m = div_ []
    [ h1_ [] [ "miso-1" ]
    , p_ [] [ text m ]
    ]
    
main :: IO ()
main = startApp App 
    { initialAction = NoOp
    , model = "hello"
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = []
    , mountPoint = Nothing
    }

