{-# LANGUAGE OverloadedStrings #-}

import           Miso
import qualified Miso.String as MS

data Person 
    = Person
        { personName :: MS.MisoString
        , personAge  :: Int
        } deriving (Eq)

type Model = [Person]

data Action
    = NoOp
    | PopOp
    deriving (Eq)

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel PopOp [] = noEff []
updateModel PopOp m = noEff (init m)

viewModel :: Model -> View Action
viewModel m = div_ []
    [ h1_ [] [ text "miso-3" ]
    , p_ [] [ button_ [ onClick PopOp ] [ text "pop" ] ]
    , ul_ [] $ map formatPerson m
    ]
    where formatPerson (Person n a) = 
            li_ [] [ text $ MS.concat [n, " (", MS.ms a, ")"] ] 

main :: IO ()
main = startApp App 
    { initialAction = NoOp
    , model = [ Person "toto" 42, Person "tata" 13, Person "tutu" 37 ]
    , update = updateModel
    , view = viewModel
    , events = defaultEvents
    , subs = []
    , mountPoint = Nothing
    }

