{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

import Data.Aeson
import GHC.Generics
import Lucid
import Network.HTTP.Media ((//), (/:))
import Network.Wai.Handler.Warp 
import Servant.Server 
import Servant

data HtmlPage

instance Accept HtmlPage where
    contentType _ = "text" // "html" /: ("charset", "utf-8")

instance ToHtml a => MimeRender HtmlPage a where
    mimeRender _ = renderBS . toHtml

data Person = Person
  { personName :: String
  , personAge  :: Int
  } deriving (Generic)

instance ToJSON Person

instance ToHtml Person where
    toHtmlRaw = toHtml
    toHtml (Person n a) = 
        span_ $ toHtml $ concat [n, " (", show a, ")"]

instance ToHtml (Maybe Person) where
    toHtmlRaw = toHtml
    toHtml (Just p) = toHtml p
    toHtml Nothing = span_ "nobody"

type PersonAPI = "person" :> Capture "id" Int 
                          :> Get '[HtmlPage, JSON] (Maybe Person)

server :: Server PersonAPI
server = return . findPerson

findPerson :: Int -> Maybe Person
findPerson 0 = Just $ Person "toto" 42
findPerson 1 = Just $ Person "titi" 7
findPerson _ = Nothing

main :: IO ()
main = run 3000 $ serve (Proxy :: Proxy PersonAPI) server

